set PATH $PATH \
         $HOME/.cargo/bin \
         /home/linuxbrew/.linuxbrew/bin \
         /opt/go/bin/ \
         $HOME/go/bin \
         /snap/bin/ \
         /usr/lib/dart/bin \
         $HOME/bin/spark/bin \
         $HOME/.pub-cache/bin \
         $HOME/bin \
         /usr/local/go/bin \
         /home/andrzej/node_modules/.bin \
         $HOME/.local/bin
          
set -g -x RUST_BACKTRACE 1

#set -g -x PREFIX_BINARY 0

set -g -x GUIX_LOCPATH $HOME/.guix-profile/lib/locale

set -g -x GOPATH $HOME/go

set -g -x GOPRIVATE gitlab.com/njskalski

alias pushall="git remote | xargs -L1 git push --all"
alias p="python3"
alias e=lino
alias cphare="ssh -L 9001:localhost:8384 andrzej@192.168.1.202"
alias cmi="ssh -L 9002:localhost:8384 andrzej@192.168.1.207"
alias addkey="sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys"
alias gowork="cd ~/r/koid"

alias szufe="ssh szuf -L 4444:localhost:4444 -L 4445:localhost:4445 -L 7657:localhost:7657 -L 8385:localhost:8384 -L 5900:localhost:5900 -X"
alias cef="e ~/.config/fish/config.fish"

alias localip="ip route get 1 | awk '{print \$7;exit}'"