function dockercp --description "copy out files from docker image to current dir"
  docker run --rm --entrypoint tar $argv[1] cC $argv[2] . | tar xvC .
end