function fish_prompt --description 'Write out the prompt'
	#Save the return status of the previous command
    set stat $status

    if not set -q __fish_prompt_normal
        set -g __fish_prompt_normal (set_color normal)
    end

    if not set -q __fish_color_blue
        set -g __fish_color_blue (set_color -o blue)
    end

    if not set -q __fish_color_yellow
        set -g __fish_color_yellow (set_color -o yellow)
    end

    #Set the color for the status depending on the value
    set __fish_color_status (set_color -o green)
    if test $stat -gt 0
        set __fish_color_status (set_color -o red)
    end

    set __parsed_time (printf "%12s" "$CMD_DURATION" | sed 's/.\{3\}/& /g' | sed 's/^[ \t]*//;s/[ \t]*$//')

    set __git_branch_wrapped ""
    set __git_branch (git rev-parse --abbrev-ref HEAD 2> /dev/null)
    if [ "$__git_branch" ]
        set __git_branch_wrapped (printf "{b: %s}" "$__git_branch")    
    end

    if not set -q __host_hash
        set __host_hash (echo -n $USER(prompt_hostname) | md5sum | cut -b 1-6)
    end

    if not set -q __fish_color_host
        set -g __fish_color_host (set_color "$__host_hash")
    end

    
    set -g __fish_prompt_cwd (set_color $fish_color_cwd)
    set -g __fish_prompt_final $__fish_prompt_normal
    
    if [ $USER = 'root' ]
        if set -q fish_color_cwd_root
            set -g __fish_prompt_cwd (set_color $fish_color_cwd_root)
        else
            set -g __fish_prompt_cwd (set_color $fish_color_cwd)
        end

        set -g __fish_prompt_final $__fish_prompt_normal(set_color -b 1c0000)
    end


    printf '[%s] %s%s@%s %s%s %s %s%s (e: %s, t: %s)\f\r%s»ʚ ' \
        (date "+%H:%M:%S") "$__fish_color_host" $USER (prompt_hostname) "$__fish_prompt_cwd" "$PWD" "$__fish_color_yellow" "$__git_branch_wrapped" "$__fish_color_status" "$stat" "$__parsed_time" "$__fish_prompt_final"

end
