function remove_wine
    rm -f ~/.config/menus/applications-merged/wine*
    rm -rf ~/.local/share/applications/wine
    rm -f ~/.local/share/desktop-directories/wine*
    rm -f ~/.local/share/icons/????_*.xpm
    rm -f ~/.local/share/icons/????_*.png
    rm -f ~/.local/share/icons/*-x-wine-*.png
    rm -f ~/.local/share/icons/*-x-wine-*.xpm
end
