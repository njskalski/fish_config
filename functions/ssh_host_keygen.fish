function ssh_host_keygen --description "generate new hostkeys in pwd"
  ssh-keygen -q -N "" -t dsa -f ssh_host_dsa_key
  ssh-keygen -q -N "" -t rsa -b 4096 -f ssh_host_rsa_key
  ssh-keygen -q -N "" -t ecdsa -f ssh_host_ecdsa_key
  ssh-keygen -q -N "" -t ed25519 -f ssh_host_ed25519_key
end